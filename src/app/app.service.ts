import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class AppService {

    constructor(private http: HttpClient) { }

    baseurl = 'http://localhost:8000/api';

    version = '/v1';

    getServersList(filters) {

        let params = new HttpParams();

        /*
        this.filters = {
            minStorage : this.storageRange[0],
            maxStorage : this.storageRange[1],
            ram : ram.join(','),
            hdd_type : this.selectedHarddiskType,
            location : this.selectedLocation
          };
          */

        if (filters.minStorage && filters.minStorage !== '' && filters.maxStorage && filters.maxStorage !== '') {
            params = params.append('minStorage', filters.minStorage);
            params = params.append('maxStorage', filters.maxStorage);
        }

        if (filters.ram && filters.ram !== '') {
            params = params.append('ram', filters.ram);
        }

        if (filters.hdd_type && filters.hdd_type !== '') {
            params = params.append('hdd_type', filters.hdd_type);
        }

        if (filters.location && filters.location !== '') {
            params = params.append('location', filters.location);
        }

        return this.http.get(this.baseurl + this.version + '/servers', { params: params });

    }

    getLocationList() {

        const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

        return this.http.get(this.baseurl + this.version + '/locations');

    }




}
