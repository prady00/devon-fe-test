import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  paginatedItemsList: any[];
  selectedLocation: any;
  selectedHarddiskType: any;
  serverList;
  filters;

  currentPage;
  totalItems = 0;

  storageRange = [ 0, 11 ];
  storageRangeText = '0 - 72TB';

  ramValues = ['2GB', '4GB', '8GB', '12GB', '16GB', '24GB', '32GB', '48GB', '64GB', '96GB'];
  storages = ['0', '250GB', '500GB', '1TB', '2TB', '3TB', '4TB', '8TB', '12TB', '24TB', '48TB', '72TB'];
  storagesValues = ['0', '250', '500', '1024', '2048', '3072', '4096', '8192', '12288', '24576', '49152', '73728'];

  locationValues = [];

  selectedRam = [];

  harddiskTypeValues = ['', 'SAS', 'SATA', 'SSD'];
  constructor(public service: AppService) {

  }

  onFiltersChange(event) {
    this.storageRangeText = '' + this.storages[this.storageRange[0]] + ' - ' + this.storages[this.storageRange[1]] + '';
    this.buildFilters();
    this.getServersList();
  }

  buildFilters() {

    console.log(this.selectedRam);

    const ram = [];

    for (const key in this.selectedRam) {
      if (this.selectedRam[key] === true) {
        ram.push(key);
      }
    }

    this.filters = {
      minStorage : this.storagesValues[this.storageRange[0]],
      maxStorage : this.storagesValues[this.storageRange[1]],
      ram : ram.join(','),
      hdd_type : this.selectedHarddiskType,
      location : this.selectedLocation
    };

  }

  getServersList() {

    this.service.getServersList(this.filters).subscribe(
      (data: any) => { this.serverList = data.data;
                      this.totalItems = data.total;
                      this.currentPage = data.current_page;

                      const a = [];

                      for (let i = 1; i < data.last_page; i++){
                        a.push(i);
                      }

                      this.paginatedItemsList = a;

                    },
      (error) => { console.log('some error occured'); }
    );

  }

  onScroll() {
    console.log('scrolled!!');
  }

  ngOnInit(): void {

    this.storageRange = [ 0, 11 ];
    this.selectedRam = [];
    this.selectedHarddiskType = '';
    this.selectedLocation = '';
    this.currentPage = 1;

    this.buildFilters();

    this.getServersList();

    this.service.getLocationList().subscribe(
      (data: any) => { this.locationValues = data; },
      (error) => { console.log('some error occured'); }
    );

  }

}
