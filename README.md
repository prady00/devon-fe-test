# Devon FE Test

This is a front end for test 

## Cloning and running on local

To clone run following command (it is presumed that npm, node and other common dependencies are already installed on system)

`git clone https://gitlab.com/prady00/devon-fe-test.git` and `cd devon-fe-test`

To run, run following command

`ng serve`

Navigate to `http://localhost:4200/`


## Running unit tests

Run `ng test` to execute the unit tests via [Karma]

